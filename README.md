**ORGANISATION D'EXAMENS ET OPTIMISATION DE LA RÉPARTITION DES MATIÈRES PAR CRÉNEAUX**

* <a href="https://www.ateliernumerique.net/organisation-examens">Interface web</a>
* <a href="https://mybinder.org/v2/git/https%3A%2F%2Fforge.apps.education.fr%2Flaurent.abbal%2Forganisation-examens/HEAD?labpath=organisation-examens.ipynb">Notebook sur Binder</a> (le lancement complet peut prendre plus d'une minute)

A faire:
 - [ ] améliorer le code
 - [ ] ajouter des formats pour les données
 - [x] ajouter une interface graphique

```
@auteurs
Laurent Abbal:
 * https://mastodon.social/@laurentabbal
 * https://twitter.com/laurentabbal
 * https://github.com/laurentabbal/
 * https://www.ateliernumerique.net/
Frédéric Junier: 
 * https://framapiaf.org/@fjunier
 * https://twitter.com/FredericJunier
 * https://github.com/frederic-junier/
 * https://frederic-junier.org/

 @licence: GPLv3 (https://www.gnu.org/licenses/gpl-3.0.html)
```
